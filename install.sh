NC='\033[m'
GREEN='\033[1;32m'
YELLOW='\033[1;33m'
RED='\033[48;2;255;0;02m'
i=1

echo -e "${YELLOW}($(( i++ )))> ${RED}Installing backend dependencies ...${NC}"
npm install \
    && echo "SUCCESS!" || echo "ERROR!"

echo -e "${YELLOW}($(( i++ )))> ${RED}Installing frontend dependencies ...${NC}"
npm install \cliente\
    && echo "SUCCESS!" || echo "ERROR!"