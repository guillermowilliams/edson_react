const md5 = require('md5');

function uploadFiles(img, folder, callback, nameFile){
    let imgDetail = {
        name        :   (nameFile ? nameFile : md5(img.name)),
        size        :   img.size,
        extension   :   img.name.split('.').pop()
    }
    let response = { 
        status:true, 
        name:imgDetail.name+'.'+imgDetail.extension,
        directory:folder+imgDetail.name+'.'+imgDetail.extension 
    };
    img.mv(folder+imgDetail.name+'.'+imgDetail.extension, function(err) {
        console.log(err)
    });
    return callback(response) 
}

module.exports = uploadFiles;