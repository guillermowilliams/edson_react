const express = require('express');
const router  = express.Router();
const verifyToken = require('../config/verifyToken')
const UploadFiles = require('../modules/UploadFiles');
const mysqlConnection = require('./../config/database');

router.get(process.env.URL_ROUTES+'schedule', verifyToken, (req, res) => {
    mysqlConnection.query('CALL sp_getSchedule();', (err, rows, fields) => {
        if(!err){
            res.json({status : rows[0]});
        }else{
            res.json({ status : false })
        }
    })
})

router.put(process.env.URL_ROUTES+'schedule/:id', verifyToken, (req, res) => {
    const { id } = req.params;
    mysqlConnection.query('CALL sp_getScheduleDetalle(?);', [id] , (err, rows, fields) => {
        if(!err){
            res.json({status : rows[0]});
        }else{
            res.json({ status : false })
        }
    })
})

router.post(process.env.URL_ROUTES+'schedule', verifyToken, (req, res) => {
    const {id, names, lastname, gender , date_of_birth, type_doc, num_doc} = req.body;
    mysqlConnection.query('CALL sp_createSchedule(?, ?, ?, ?, ?, ?, ?);', [
        id,
        names,
        lastname,
        gender,
        date_of_birth,
        type_doc,
        num_doc,
    ], (err, rows, fields) => {
        if(!err){
            res.json({status : rows[0]});
        }else{
            res.json({ status : false })
        }
    })
})

router.delete(process.env.URL_ROUTES+'schedule', verifyToken, (req, res) => {
    const { id } = req.body;
    mysqlConnection.query('CALL sp_deleteSchedule(?);', [
        id
    ], (err, rows, fields) => {
        if(!err){
            res.json({status : rows[0]});
        }else{
            res.json({ status : false })
        }
    })
})

router.put(process.env.URL_ROUTES+'scheduleImg/:id', verifyToken, (req, res) => {
    const { id } = req.params;
    let Image1Res = false;
    
    if (req.files || Object.keys(req.files).length > 1) {
        UploadFiles(
            req.files.imgSchedule, 
            './cliente/public/img/user/', 
            function(response){
                Image1Res = response
            },
            'img'+id,
        )
        if(Image1Res.status){
            mysqlConnection.query('CALL sp_updateImgSchedule(?, ?);', [
                id,
                '/img/user/'+Image1Res.name
            ], (err, rows, fields) => {
                if(!err){
                    res.json({status : rows[0]});
                }else{
                    res.json({ status : false })
                }
            })
        }else{
            return res.status(500).send('Error fallo la carga de imagenes.');
        }
    }
})

module.exports = router;