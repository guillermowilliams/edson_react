const express = require('express');
const jwt = require('jsonwebtoken')
const router  = express.Router();
const passwordHash = require('password-hash');
const mysqlConnection = require('./../config/database');

function comprobanteUser(email, callback){
    const query = 'SELECT password FROM users WHERE email = ? AND status = 1';
    mysqlConnection.query(query, [ email ] , (err, rows, fields) => {
        if(!err){
            let verify = false;
            if (rows[0]) {
                verify = rows[0].password;
            }
            return callback(verify);
        }
    })
}

function internalUserDetails(email, callback){
    const query = `CALL sp_getInternalUser(?);`;
    mysqlConnection.query(query, [
        email
    ], (err, rows, fields) => {
        if(!err){
            let res = JSON.parse(JSON.stringify(rows))[0][0];
            let infoUser = {
                name: res.name,
                lastname: res.lastname,
                email: res.email
            }
            jwt.sign({infoUser}, process.env.SITE_KEY, (err, token) => {
                return callback({status:token})
            })
        }
    })
}

router.post(process.env.URL_ROUTES+'login', (req, res) => {
    const {email , password} = req.body;
    comprobanteUser(email, function(result){
        if (passwordHash.verify(password, result)) {
            internalUserDetails(email,function(result){
                res.json(result);
            })
        }else{
            res.json({ status: false });
        }
    });
})

router.post(process.env.URL_ROUTES+'signup', (req, res) => {
    const {names , lastname, email, password} = req.body;
    const query = 'CALL sp_createUser(?, ?, ?, ?);';
    mysqlConnection.query(query, [
        names,
        lastname,
        email,
        passwordHash.generate(password)
    ], (err, rows, fields) => {
        if(!err){
            if (rows[0]) {
                internalUserDetails(email,(result) =>{
                    res.json(result);
                })
            }else{
                res.json(false)
            }
        }
    })
})

module.exports = router;