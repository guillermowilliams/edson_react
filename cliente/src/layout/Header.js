import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';
import jwt from "jsonwebtoken"
import { NavHashLink } from "react-router-hash-link"
import { connect } from 'react-redux'

const mapStateToProps = (state) => ({ sesion:state.sesion })
const mapDispatchToProps = dispatch => {
    return{
        loged: () => dispatch({ type:'LOGED' }),
        logout: () => dispatch({ type:'LOGOUT' })
    }
}

const MenuPerfil = props => {
    var userData = jwt.decode(JSON.parse(localStorage.getItem('token')).status).infoUser;
    let name = userData.name.split(" ");
    let lastname = userData.lastname.split(" ");
    return (
        <Nav navbar>
            <UncontrolledDropdown nav inNavbar>
            <DropdownToggle nav caret>
                { name[0]+' '+lastname[0] }
            </DropdownToggle>
            <DropdownMenu right>
                <DropdownItem onClick={(e) => {localStorage.removeItem('token'); props.preps.logout()}}>
                Cerrar sesión
                </DropdownItem>
            </DropdownMenu>
            </UncontrolledDropdown>
        </Nav>
    )
}

const Header = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar color="primary" dark expand="md">
        <div className="container">
            <NavHashLink className="navbar-brand" exact to="/agenda">Sistema</NavHashLink>
            <NavbarToggler onClick={toggle} />
            <Collapse isOpen={isOpen} navbar>
            <Nav className="mr-auto" navbar>
                <NavItem>
                    <NavHashLink to="/agenda" exact className="nav-link">Agenda</NavHashLink>
                </NavItem>
            </Nav>
            {
                (!props.sesion ? null : <MenuPerfil preps={props}/>)
            }
            
            </Collapse>
        </div>
      </Navbar>
    </div>
  );
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Header)