import React, { Component } from 'react'
import {Redirect, Link} from 'react-router-dom';
import { connect } from 'react-redux'

const mapStateToProps = (state) => ({ sesion:state.sesion })
const mapDispatchToProps = dispatch => {
    return{
        loged: () => dispatch({ type:'LOGED' }),
        logout: () => dispatch({ type:'LOGOUT' })
    }
}

class WebLayout extends Component {
    componentWillMount = () =>{
        document.title = this.props.title;
    }
    render() {
        if (!this.props.sesion) {
            return (<Redirect to={'/login'} />)
        }
        return (
            <div className={this.props.className}>
                <br/>
                <div className="container">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <Link to={"/"}>Dashboard</Link>
                            </li>
                            { this.props.page ? <li class="breadcrumb-item active" aria-current="page">{this.props.page}</li> : null }
                        </ol>
                    </nav>
                </div>
                {
                    this.props.children
                }
            </div>
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WebLayout)
