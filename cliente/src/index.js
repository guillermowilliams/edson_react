import React from 'react';
import { render } from 'react-dom';
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import $ from "jquery"

import '@fortawesome/fontawesome-free/css/all.min.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'assets/scss/index.scss';
import 'datatables.net-bs4/css/dataTables.bootstrap4.min.css'

import MyRouter from 'router/myRouter';
import sesion from 'reducers'
import * as serviceWorker from './serviceWorker';

window.$ = $;
window.jQuery = window.$;

const store = createStore(sesion)

render(
    <Provider store={store}>
        <MyRouter />
    </Provider>, 
    document.getElementById('root')
);

serviceWorker.unregister();
