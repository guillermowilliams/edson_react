import React, { Component, Suspense } from 'react'
import { BrowserRouter as Router, Route, Switch} from "react-router-dom";
import { 
    Registrar,
    Agenda,
    Login,
    AgendaDetalle
} from "views"
import Header from "layout/Header"
import ScrollToTop from "component/others/ScrollToTop"

export default class myRouter extends Component {
    render() {
        return (
            <Router>
                <ScrollToTop>
                    <Suspense fallback={<div />}>
                        <Switch>
                            <Route path="/login">
                                <Login />
                            </Route>
                            <Route path="/registrar">
                                <Registrar />
                            </Route>
                            <Route path="/">
                                <Header />
                                <Route path="/" exact component={Agenda} />
                                <Route path="/agenda" exact component={Agenda} />
                                <Route path="/agenda/detalle/:id" exact component={AgendaDetalle} />
                            </Route>
                        </Switch>
                    </Suspense>
                </ScrollToTop>
            </Router>
        )
    }
}
