import React, { Component, Fragment } from 'react'

export class Input extends Component {
    onEnter = (e) => {
        if (e.which == 13) { this.props.submitForm() }
    }

    render() {
        const { 
            type, 
            name, 
            className, 
            id, 
            placeholder, 
            value, 
            onChange, 
            condition, 
            validator,
            startValid
        } = this.props;
        
        return (
            <Fragment>
                <input
                    type={type}
                    name={name}
                    className={className+" "+(startValid ? (validator.fieldValid(name)  ? "is-valid" : "is-invalid") : "")}
                    id={id}
                    placeholder={placeholder}
                    value={value}
                    onChange={(e) => onChange(e)}
                    onKeyPress={(e) => this.onEnter(e)}
                />
                {validator.message(name, value, condition)}
            </Fragment>
        )
    }
}
