import React, { Component, Fragment } from 'react'

export class SelectBox extends Component {
    render() {
        const { 
            name, 
            className, 
            id, 
            value, 
            onChange, 
            condition, 
            validator,
            startValid,
            options
        } = this.props;

        return (
            <Fragment>
                <select 
                    name={name}
                    className={className+" "+(startValid ? (validator.fieldValid(name)  ? "is-valid" : "is-invalid") : "")}
                    id={id}
                    value={value}
                    onChange={(e) => onChange(e)}
                >
                    {
                        options.map((fill, index) => 
                            <option value={fill.value}>{fill.text}</option>
                        )
                    }
                </select>
                {validator.message(name, value, condition)}
            </Fragment>
        )
    }
}
