import React, { Component, Fragment } from 'react'
import { Input, SelectBox } from "component/others"

export class Formulario extends Component {
    render() {
        const { startValid, id, names, lastname, gender, date_of_birth, type_doc, num_doc } = this.props.thes
        return (
            <Fragment>
                <Input 
                    type="hidden" 
                    name="id"
                    id="id" 
                    className={"form-control form-control-sm"}  
                    value={id}
                    onChange={this.props.onChange}
                    validator={this.props.validator}
                    condition={""}
                    startValid={startValid}
                    submitForm={this.props.submitForm}
                />
                <div className="form-group">
                    <Input 
                        type="text" 
                        name="names"
                        id="names" 
                        className={"form-control form-control-sm"}  
                        placeholder="Nombres" 
                        value={names}
                        onChange={this.props.onChange}
                        validator={this.props.validator}
                        condition={"required|min:3"}
                        startValid={startValid}
                        submitForm={this.props.submitForm}
                    />
                </div>
                <div className="form-group">
                    <Input 
                        type="text" 
                        name="lastname"
                        id="lastname" 
                        className={"form-control form-control-sm"}  
                        placeholder="Apellidos" 
                        value={lastname}
                        onChange={this.props.onChange}
                        validator={this.props.validator}
                        condition={"required|min:3"}
                        startValid={startValid}
                        submitForm={this.props.submitForm}
                    />
                </div>
                <div className="form-group">
                    <SelectBox 
                        name="gender"
                        id="gender" 
                        className={"form-control form-control-sm"}  
                        value={gender}
                        onChange={this.props.onChange}
                        validator={this.props.validator}
                        condition={"required|min:2"}
                        startValid={startValid}
                        options = {[
                            {'value':'', 'text':'Seleccione género'},
                            {'value':'masculino', 'text':'Masculino'},
                            {'value':'femenino', 'text':'Femenino'},
                            {'value':'otros', 'text':'Otros'},
                        ]}
                    />
                </div>
                <div className="form-group">
                    <label className="control-label">Fecha de nacimiento</label>
                    <Input 
                        type="date" 
                        name="date_of_birth"
                        id="date_of_birth" 
                        className={"form-control form-control-sm"}  
                        placeholder="Apellidos" 
                        value={date_of_birth}
                        onChange={this.props.onChange}
                        validator={this.props.validator}
                        condition={"required"}
                        startValid={startValid}
                        submitForm={this.props.submitForm}
                    />
                </div>
                <div className="form-group">
                    <label className="control-label">Tipo de documento</label>
                    <SelectBox 
                        name="type_doc"
                        id="type_doc" 
                        className={"form-control form-control-sm"}  
                        value={type_doc}
                        onChange={this.props.onChange}
                        validator={this.props.validator}
                        condition={"required|min:2"}
                        startValid={startValid}
                        options = {[
                            {'value':'', 'text':'Seleccione tipo de documento'},
                            {'value':'L.E / DNI', 'text':'Libreta electoral o DNI'},
                            {'value':'PASAPORTE', 'text':'PASAPORTE'},
                            {'value':'CARNET EXT.', 'text':'Carnet de extranjeria'},
                            {'value':'RUC', 'text':'Reg. unico de contribuyentes'},
                        ]}
                    />
                </div>
                <div className="form-group">
                    <Input 
                        type="number" 
                        name="num_doc"
                        id="num_doc" 
                        className={"form-control form-control-sm"}  
                        placeholder="Nro de documento" 
                        value={num_doc}
                        onChange={this.props.onChange}
                        validator={this.props.validator}
                        condition={"required|min:3"}
                        startValid={startValid}
                        submitForm={this.props.submitForm}
                    />
                </div>
            </Fragment>
        )
    }
}
