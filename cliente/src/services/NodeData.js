import axios from "axios"

export async function NodeData(action, method, userData, callback){
    let token = '';
    if (localStorage.getItem('token')) {
        token = "Bearer "+ JSON.parse(localStorage.getItem('token')).status;
    }
    await axios({
        method: method,
        headers: {
            'Authorization':token
        },
        url: action,
        data:userData
    })
    .then(function (result) {
        return callback(result)
    })
    .catch(function (error) {
        console.log(error);
    });
}