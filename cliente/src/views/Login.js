import React, { Component } from 'react'
import { Link } from "react-router-dom"
import SimpleReactValidator from "simple-react-validator"
import { Input } from "component/others/Input"
import {Redirect} from 'react-router-dom';
import { NodeData } from "services/NodeData"
import { connect } from 'react-redux'
import Swal from "sweetalert2"

const mapStateToProps = (state) => ({ sesion:state.sesion })
const mapDispatchToProps = dispatch => {
    return{
        loged: () => dispatch({ type:'LOGED' }),
        logout: () => dispatch({ type:'LOGOUT' })
    }
}

class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            startValid: false,
            email:'',
            password:'',
        }
        this.validator = new SimpleReactValidator();
        this.login = this.login.bind(this);
    }

    clearInput = () => {
        this.setState({
            startValid: false,
            email:'',
            password:'',
        })
        const submit_btn = window.$('.btn-login')
        submit_btn.removeAttr('disabled').removeClass('disabled');
        submit_btn.text('Ingresar')
    }

    onChange = (event) =>{
        var obj = {};
        obj[event.target.name] = event.target.value;
        this.setState(obj);
    }

    submitForm = () =>{
        this.validator.allValid() ? this.login() : this.setState({ startValid: true});
    }

    login = () => {
        const submit_btn = window.$('.btn-login')
        submit_btn.attr('disabled', 'true').addClass('disabled');
        submit_btn.html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>`)

        let json = {
            email: this.state.email,
            password: this.state.password
        }

        NodeData('/login', 'post', json, (result) => {
            if(result.data.status){
                localStorage.setItem('token',JSON.stringify(result.data));
                this.props.loged()
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Direccion de correo electronico o contraseña incorrectos.'
                })
            }
            this.clearInput();
        });
    }

    render() {
        if (this.props.sesion) {
            return (<Redirect to={'/agenda'} />)
        }
        return (
            <div className="page-login">
                <form className="form-signin">
                    <img className="mb-4" src="https://getbootstrap.com/docs/4.4/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72" />
                    <h1 className="h3 mb-3 font-weight-normal">Por favor inicie sesión</h1>
                    <Input
                        type="email" 
                        name="email"
                        id="inputEmail" 
                        className={"form-control"}  
                        placeholder="Email" 
                        value={this.state.email}
                        onChange={this.onChange}
                        validator={this.validator}
                        condition={"required|email"}
                        startValid={this.state.startValid}
                        submitForm={this.submitForm}
                    />
                    <Input
                        type="password" 
                        name="password"
                        id="inputPassword" 
                        className={"form-control "+(this.state.startValid ? (this.validator.fieldValid('password')  ? "" : "is-invalid") : "")} 
                        placeholder="Contraseña" 
                        value={this.state.password}
                        onChange={this.onChange}
                        validator={this.validator}
                        condition={"required"}
                        startValid={this.state.startValid}
                        submitForm={this.submitForm}
                    />
                    <button 
                        type="button"
                        className="btn btn-lg btn-primary btn-block btn-login" 
                        onClick={(e) => this.submitForm(e)}
                    >
                        Ingresar
                    </button>
                    <p className="text-center mt-3">
                        No dispones de una cuenta <Link to={"/registrar"}>regístrate.</Link>
                    </p>
                    <p className="mt-5 mb-3 text-muted">&copy; 2020</p>
                </form>
            </div>
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login)