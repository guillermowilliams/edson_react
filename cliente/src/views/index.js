import {lazy} from "react"

export const Dashboard= lazy(()=> import(/* webpackChunkName: "dashboard" */"./Dashboard"))
export const Agenda= lazy(()=> import(/* webpackChunkName: "agenda" */"./Agenda"))
export const Login= lazy(()=> import(/* webpackChunkName: "login" */"./Login"))
export const Registrar= lazy(()=> import(/* webpackChunkName: "registrar" */"./Registrar"))
export const AgendaDetalle= lazy(()=> import(/* webpackChunkName: "agenda_detalle" */"./AgendaDetalle"))