import React, { Component } from 'react'
import WebLayout from "layout/WebLayout"

export default class Dashboard extends Component {
    render() {
        return (
            <WebLayout className="page-dashboard" title="Proyecto | Dashboard">
                <div className="container">
                    Hola mundo
                </div>
            </WebLayout>
        )
    }
}
