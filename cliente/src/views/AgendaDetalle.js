import React, { Component } from 'react'
import { Link } from "react-router-dom"
import { NodeData } from "services/NodeData"
import WebLayout from "layout/WebLayout"

let form = new FormData();
const $ = window.$;

export default class AgendaDetalle extends Component {
    constructor(props){
        super(props);
        this.state = {
            names : null,
            lastname : null,
            birth: null,
            gender: null,
            date_of_birth: null,
            type_doc: null,
            num_doc: null,
            date_register: null,
            imgSchedule: null
        }
    }

    componentDidMount  = () => {
        this.fnAsync();
    }

    fnAsync = async () => {
        await this.fnGetInformation();
    }

    fnCalculateBirth = (fecha) => {
        var hoy = new Date();
        var cumpleanos = new Date(fecha);
        var edad = hoy.getFullYear() - cumpleanos.getFullYear();
        var m = hoy.getMonth() - cumpleanos.getMonth();
    
        if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
            edad--;
        }
    
        return edad;
    }

    fnGetInformation = async () => {
        let id = this.props.match.params.id;
        await NodeData('/schedule/'+id, 'put', {}, (result) => {
            if(result.data.status){
                let data = result.data.status[0];
                this.setState({ 
                    names : data.names,
                    lastname : data.lastname,
                    birth: this.fnCalculateBirth(data.birth),
                    gender: data.gender,
                    date_of_birth: data.date_of_birth,
                    type_doc: data.type_doc,
                    num_doc: data.num_doc,
                    date_register: data.date_register,
                    imgSchedule: data.user_img
                })
            }else{
                alert('Problemas al obtener los datos.')
            }
        });
    }

    fileSelectedHandler = (e) =>{
        var obj = {};
        if (e.target.files.length > 0) {
            obj[e.target.name] = URL.createObjectURL(e.target.files[0]);
            this.setState(obj);
            
            form.append(e.target.name,e.target.files[0])
        }
    }

    subirImagen = (e) => {
        let id = this.props.match.params.id;
        $(e.currentTarget).attr('disabled','true').addClass('disabled')
        NodeData('/scheduleImg/'+id,'put', form, function(result){
            if (result) {
                window.location.reload();
            }
        })
    }

    render() {
        return (
            <WebLayout className="page-agenda" title="Proyecto | Agenda" page={"Agenda"}>
                <div className="container pb-4">
                    <br/>
                    <div className="row">
                        <div className="col-md-4">
                            <div className="card">
                                <div className="card-header">
                                    <strong>Imagen</strong>
                                </div>
                                <div className="card-body d-flex justify-content-center flex-column align-items-center">
                                    <div 
                                        className="img-schedule imgSchedule bg-contain" 
                                        style={{backgroundImage:`url(${this.state.imgSchedule})`, width:150, height: 150}} 
                                    />
                                    <br/>
                                    <input 
                                        type="file" 
                                        name="imgSchedule" 
                                        onChange={(e) => this.fileSelectedHandler(e)}
                                        accept="image/jpg, image/jpeg, image/png"
                                    />
                                </div>
                                <div className="card-footer">
                                    <button 
                                        type="button"
                                        className="btn btn-success btn-sm"
                                        onClick={(e) => this.subirImagen(e)}
                                    >
                                        <i className="fas fa-save"></i>&nbsp;&nbsp;Guardar imagen
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-8">
                            <div className="card">
                                <div className="card-header">
                                <strong>{this.state.names} {this.state.lastname}</strong>
                                </div>
                                <div className="card-body">
                                    <p className="lead">
                                        <ul>
                                            <li>Edad : {this.state.birth}</li>
                                            <li>Género : <span className="text-uppercase">{this.state.gender}</span></li>
                                            <li>Fecha de nacimiento : {this.state.date_of_birth}</li>
                                            <li>Documento de identidad : {this.state.type_doc} - {this.state.num_doc}</li>
                                            <li>Fecha de registro : {this.state.date_register}</li>
                                        </ul>
                                    </p>
                                </div>
                                <div className="card-footer">
                                    <Link className="btn btn-primary btn-sm" to={"/agenda"} role="button">
                                        <i className="fas fa-arrow-circle-left"></i>&nbsp;
                                        Regresar
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </WebLayout>
        )
    }
}
