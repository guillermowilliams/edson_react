import React, { Component } from 'react'
import { Link } from "react-router-dom"
import SimpleReactValidator from "simple-react-validator"
import WebLayout from "layout/WebLayout"
import { NodeData } from "services/NodeData"
import DataTable from "datatables"
import DataTableBs4 from "datatables.net-bs4"
import json from "component/others/DataTableSpanish.json"
import { Formulario } from "component/agenda"
import Swal from "sweetalert2"

const $ = window.$;

export default class Agenda extends Component {
    constructor(props){
        super(props);
        this.state = {
            dataSchedule:[],
            startValid: false,
            id:'',
            names:'',
            lastname:'',
            gender:'',
            date_of_birth:'',
            type_doc:'',
            num_doc:''
        }
        this.validator = new SimpleReactValidator();
    }

    componentDidMount = async () => {
        await this.fnObtainData();
    }

    fnClearData = () => {
        this.setState({
            startValid: false,
            id:'',
            names:'',
            lastname:'',
            gender:'',
            date_of_birth:'',
            type_doc:'',
            num_doc:''
        })
    }

    fnObtainData = async () =>{
        await NodeData('/schedule', 'get', json, (result) => {
            if(result.data.status){
                console.log(result.data.status)
                this.setState({ dataSchedule: result.data.status})
            }else{
                alert('Problemas al obtener los datos.')
            }
        });
    }

    fnCalculateBirth = (fecha) => {
        var hoy = new Date();
        var cumpleanos = new Date(fecha);
        var edad = hoy.getFullYear() - cumpleanos.getFullYear();
        var m = hoy.getMonth() - cumpleanos.getMonth();
    
        if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
            edad--;
        }
    
        return edad;
    }

    onChange = (event) =>{
        var obj = {};
        obj[event.target.name] = event.target.value;
        this.setState(obj);
    }

    submitForm = () =>{
        this.validator.allValid() ? this.AgendarEjecution() : this.setState({ startValid: true});
    }

    fnUpdateState = (id, names, lastname, gender, date_of_birth, type_doc, num_doc) =>{
        this.setState({
            id:id,
            names:names,
            lastname:lastname,
            gender:gender,
            date_of_birth:date_of_birth,
            type_doc:type_doc,
            num_doc:num_doc
        })
        setTimeout(() => {
            this.setState({ startValid: true })
        }, 500);
    }

    AgendarEjecution = () => {
        let element = $('.card-formulario .btn');
        element.attr('disabled', 'true').addClass('disabled');
        element.find('.spinner-border').removeClass('d-none')
    
        let json = {
            id:this.state.id,
            names:this.state.names,
            lastname:this.state.lastname,
            gender:this.state.gender,
            date_of_birth:this.state.date_of_birth,
            type_doc:this.state.type_doc,
            num_doc:this.state.num_doc
        }

        NodeData('/schedule', 'post', json, (result) => {
            if(result.data.status){
                this.fnObtainData();
                this.fnClearData();
            }else{
                Swal.fire({
                    icon: 'error',
                    title: 'Error',
                    text: 'Ocurrio un error al ejecutar la operación puede deberse a que el documento de identidad que esta tratando de ingresar ya se encuentra registrado.'
                })
            }
            element.removeAttr('disabled').removeClass('disabled');
            element.find('.spinner-border').addClass('d-none')
        });
    }

    deleteData = (id) => {
        Swal.fire({
            title: '¿Esta seguro que desea eliminar?',
            text: "El dato que esta requiriendo eliminar se eliminara permanentemente.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Eliminar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {
                NodeData('/schedule', 'delete', { id:id }, (response) => {
                    if(response.data.status){
                        console.log(response.data.status)
                        Swal.fire(
                            'Eliminado!',
                            'El dato ah sido eliminado correctamente.',
                            'success'
                        )
                        this.fnObtainData();
                        this.fnClearData();
                    }else{
                        alert('Error al procesar la información')
                    }
                });
            }
        })
    }

    render() {
        return (
            <WebLayout className="page-agenda" title="Proyecto | Agenda" page={"Agenda"}>
                <div className="container pb-4">
                    <br />
                    <div className="row">
                        <div className="col-md-4">
                            <div className="card card-formulario shadow">
                                <div className="card-header d-flex align-items-center justify-content-between">
                                    <strong>Formulario</strong>
                                </div>
                                <div className="card-body">
                                    <Formulario 
                                        thes={this.state}
                                        onChange={this.onChange}
                                        validator={this.validator}
                                        submitForm={this.submitForm}
                                    />
                                </div>
                                <div className="card-footer">
                                    <button 
                                        type="button"
                                        className={"btn btn-sm "+(this.state.id == '' ? 'btn-primary' : 'btn-success')}
                                        onClick={(e) => this.submitForm(e)}
                                    >
                                        <span class="spinner-border spinner-border-sm d-none" role="status" aria-hidden="true"></span> Guardar
                                    </button>
                                    <button 
                                        className={"btn btn-danger btn-sm ml-3 "+(this.state.id == '' ? 'd-none' : '')} 
                                        onClick={(e) => this.fnClearData(e)}
                                    >
                                        Cancelar
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-8">
                            <div className="card shadow">
                                <div className="card-header d-flex align-items-center justify-content-between">
                                    <strong><i className="fas fa-user"></i> &nbsp;Agenda</strong>
                                </div>
                                <div className="card-body">
                                    <div className="table-responsive">
                                        <table className="table table-hover table-sm table-bordered dataTable-web">
                                            <thead>
                                                <tr>
                                                    <th>Nombres</th>
                                                    <th>Apellidos</th>
                                                    <th>Edad</th>
                                                    <th>Documento de identidad</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    this.state.dataSchedule.map((fill,id) => 
                                                        <tr key={id}>
                                                            <td className="text-capitalize">{fill.names}</td>
                                                            <td className="text-capitalize">{fill.lastname}</td>
                                                            <td>{this.fnCalculateBirth(fill.birth)}</td>
                                                            <td className="text-uppercase">{fill.type_doc} - {fill.num_doc}</td>
                                                            <td>
                                                                <button
                                                                    onClick={(e) => this.deleteData(fill.id_schedule)} 
                                                                    className="btn btn-sm btn-danger"
                                                                >
                                                                    <i className="fas fa-trash"></i>
                                                                </button>
                                                                <button
                                                                    onClick={(e) => this.fnUpdateState(
                                                                        fill.id_schedule,
                                                                        fill.names,
                                                                        fill.lastname,
                                                                        fill.gender,
                                                                        fill.birth,
                                                                        fill.type_doc,
                                                                        fill.num_doc
                                                                    )} 
                                                                    className="btn btn-sm btn-success ml-3"
                                                                >
                                                                    <i className="fas fa-pen"></i>
                                                                </button>
                                                                <Link to={"/agenda/detalle/"+fill.id_schedule} className="btn btn-warning btn-sm ml-3">
                                                                    <i className="fas fa-eye" />
                                                                </Link>
                                                            </td>
                                                        </tr>
                                                    )
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </WebLayout>
        )
    }
}
