const express = require('express');
const fileUpload = require('express-fileupload');
require('dotenv').config()
const app = express();

// Settings
app.set('port', 3002);

// Middlewares
app.use(fileUpload());
app.use(express.json());

//Routes
app.use(require('./src/routes/user'));
app.use(require('./src/routes/schedule'));

app.listen(app.get('port'), () => {
    console.log('Server on port 3002')
})